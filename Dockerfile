FROM phusion/baseimage
LABEL maintainer="Gwen Dawes"

#Base installs
RUN apt-get update
RUN apt-get -y install nano
RUN apt-get -y install net-tools
RUN apt-get -y install nmap

#SSH stuff
RUN rm -f /etc/service/sshd/down
COPY id_rsa.pub /tmp/id_rsa.pub
RUN cat /tmp/id_rsa.pub >> /root/.ssh/authorized_keys
RUN chmod 600 /root/.ssh/authorized_keys

EXPOSE 22/tcp
CMD ["/sbin/my_init"]

